import csv


def get_courses_list():
    with open('courses/description.csv', encoding='utf-8') as file:
        csvreader = csv.DictReader(file, delimiter=';')
        courses_list = []
        for row in csvreader:
            course_dict = {
                'id': row['id'],
                'course_name': row['course_name'],
                'folder_name': row['folder_name'],
                'description': row['description'],
                'length': row['length'],
                'author': row['author']
            }
            courses_list.append(course_dict)
    return courses_list


def get_course_name_by_id(id):
    courses_list = get_courses_list()
    course_name = ''
    for course in courses_list:
        if course['id'] == id:
            course_name = course['course_name']
            break

    return course_name


def get_course_name_by_folder_name(folder_name):
    courses_list = get_courses_list()
    course_name = ''
    for course in courses_list:
        if course['folder_name'] == folder_name:
            course_name = course['course_name']
            break
    return course_name


def get_course_description_by_course_name(name):
    courses_list = get_courses_list()
    course_description = ''
    for course in courses_list:
        if course['course_name'] == name:
            course_description = course['description']
            break
    return course_description


def get_folder_name_by_course_name(name):
    courses_list = get_courses_list()
    folder_name = ''
    for course in courses_list:
        if course['course_name'] == name:
            folder_name = course['folder_name']
            break
    return folder_name


def get_folder_name_by_course_id(course_id):
    courses_list = get_courses_list()
    folder_name = ''
    for course in courses_list:
        if course['id'] == course_id:
            folder_name = course['folder_name']
            break
    return folder_name


def get_course_id_by_folder_name(folder_name):
    courses_list = get_courses_list()
    course_id = ''
    for course in courses_list:
        if course['folder_name'] == folder_name:
            course_id = course['id']
            break
    return course_id


def get_course_id_by_course_name(course_name):
    courses_list = get_courses_list()
    course_id = ''
    for course in courses_list:
        if course['course_name'] == course_name:
            course_id = course['id']
            break
    return course_id


def get_course_author_by_id(course_id):
    courses_list = get_courses_list()
    author = ''
    for course in courses_list:
        if course['id'] == course_id:
            author = course['author']
            break
    return author


def get_lesson_dict(course_id, lesson_number):
    folder_name = get_folder_name_by_course_name(
        get_course_name_by_id(course_id)
    )
    lesson_number = lesson_number + 1
    with open(f'courses/{folder_name}/description.csv', encoding='utf-8') \
            as file:
        csvreader = list(csv.reader(file, delimiter=';'))
        lesson_dict = {
            'number': csvreader[lesson_number][0],
            'filename': csvreader[lesson_number][1],
            'lesson_name': csvreader[lesson_number][2],
            'annotation': csvreader[lesson_number][3]
        }
    return lesson_dict


def get_course_lenght(folder_name):
    with open(f'courses/{folder_name}/description.csv', encoding='utf-8') \
            as file:
        csvreader = list(csv.reader(file, delimiter=';'))
        course_lenght = str(len(csvreader) - 1)
        return course_lenght
