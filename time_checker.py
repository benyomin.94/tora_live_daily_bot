import lessons
import db_operations
import holiday_checker

from time import sleep


holiday_status = holiday_checker.check_date()
if not holiday_status:
    subscription_list = db_operations.get_actual_subscriptions()
    if subscription_list:
        for subscription_id in subscription_list:
            lessons.send_audio_lesson(subscription_id)
            sleep(0.2)
