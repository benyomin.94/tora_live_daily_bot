import psycopg2
from datetime import datetime

import settings
import csv_operations as c


def check_user(user):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT user_id FROM users WHERE user_id = \'{user.id}\''
            cur.execute(query)
            user_in_db = cur.fetchone()
            if not user_in_db:
                if not user.first_name:
                    user.first_name = 'NULL'
                if not user.last_name:
                    user.last_name = 'NULL'
                if not user.username:
                    user.username = 'NULL'
                query = f'INSERT INTO users VALUES (' \
                        f'\'{user.id}\', ' \
                        f'\'{user.first_name}\', ' \
                        f'\'{user.last_name}\',' \
                        f'\'{user.username}\')'
                cur.execute(query)
                conn.commit()


def subscribe(user, folder_name):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            course_id = c.get_course_id_by_folder_name(folder_name)
            subscription_id = f'{user}/{course_id}'

            check_subs_query = f'SELECT subscription_id ' \
                               f'FROM subscriptions WHERE ' \
                               f'subscription_id = \'{subscription_id}\''

            cur.execute(check_subs_query)
            subscription_is_exist = cur.fetchone()
            status = None

            if not subscription_is_exist:
                    query = f'INSERT INTO subscriptions ' \
                            f'VALUES (\'{user}\',' \
                            f'        \'{subscription_id}\',' \
                            f'        \'on\',' \
                            f'        \'10:00\')'
                    cur.execute(query)
                    conn.commit()
                    status = 'started'
            else:
                subscription_is_active_query = f'SELECT subscription_status ' \
                                               f'FROM subscriptions WHERE ' \
                                               f'subscription_id = ' \
                                               f'\'{subscription_id}\''
                cur.execute(subscription_is_active_query)
                subscription_is_active = cur.fetchone()
                if subscription_is_active[0] == 'off':
                    activate_subsciption_query = f'UPDATE subscriptions SET ' \
                                                 f'subscription_status = \'on\' ' \
                                                 f'WHERE subscription_id = ' \
                                                 f'\'{subscription_id}\''
                    cur.execute(activate_subsciption_query)
                    conn.commit()
                    status = 'continued'
                elif subscription_is_active[0] == 'on':
                    status = False
            return status


def stop_subscription(user, folder_name):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            subscription_id = \
                f'{user}/{c.get_course_id_by_folder_name(folder_name)}'
            query = f'UPDATE subscriptions SET subscription_status = \'off\' ' \
                    f'WHERE subscription_id = \'{subscription_id}\''
            cur.execute(query)
            conn.commit()


def delete_subscription(user, folder_name):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            course_id = c.get_course_id_by_folder_name(folder_name)
            subscription_id = f'{user}/{course_id}'
            query = f'DELETE FROM subscriptions WHERE subscription_id = ' \
                    f'\'{subscription_id}\''
            cur.execute(query)
            query = f'DELETE FROM last_listened WHERE subscription_id = ' \
                    f'\'{subscription_id}\''
            cur.execute(query)
            query = f'DELETE FROM got_lesson_today ' \
                    f'WHERE subscription_id = \'{subscription_id}\''
            cur.execute(query)
            conn.commit()


def edit_time(subsription_id, time):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            edit_time_query = f'UPDATE subscriptions SET ' \
                              f'publication_time = \'{time}\'' \
                              f'WHERE subscription_id = \'{subsription_id}\''
            cur.execute(edit_time_query)
            conn.commit()


def get_publication_time(subscription_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT publication_time FROM subscriptions ' \
                    f'WHERE subscription_id = \'{subscription_id}\''
            cur.execute(query)
            time = str(cur.fetchone()[0])[:-3]
            return time


def get_subscription_list(user):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT * FROM subscriptions ' \
                    f'WHERE subscriptions.user = \'{user}\''
            cur.execute(query)
            subscriptions = cur.fetchall()
            subscription_list = list()
            for subscription in subscriptions:
                sub = dict()
                sub['course_id'] = subscription[1].split('/')[1]
                sub['course_name'] = c.get_course_name_by_id(sub['course_id'])
                sub['status'] = subscription[2]
                sub['time'] = str(subscription[3])[:-3]
                subscription_list.append(sub)
            return subscription_list


def get_lesson_dict(subscription_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT * FROM last_listened ' \
                    f'WHERE last_listened.subscription_id = ' \
                    f'\'{subscription_id}\''
            cur.execute(query)
            lesson_data = cur.fetchone()
            course_id = subscription_id.split('/')[1]
            if lesson_data:
                lesson_number = int(lesson_data[2])
                lesson_dict = c.get_lesson_dict(course_id, lesson_number)
            else:
                lesson_dict = c.get_lesson_dict(course_id, 0)
            return lesson_dict


def increment_lesson_number(subscription_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT lesson_number FROM last_listened ' \
                    f'WHERE subscription_id = \'{subscription_id}\''
            cur.execute(query)
            lesson_number = cur.fetchone()
            if lesson_number:
                new_lesson_number = str(int(lesson_number[0]) + 1)
                query = f'UPDATE last_listened ' \
                        f'SET lesson_number = \'{new_lesson_number}\', ' \
                        f'updated_datetime = \'{datetime.now()}\' ' \
                        f'WHERE subscription_id = \'{subscription_id}\''
                cur.execute(query)
                conn.commit()
            else:
                user = subscription_id.split('/')[0]
                query = f'INSERT INTO last_listened VALUES (' \
                        f'\'{user}\', \'{subscription_id}\', ' \
                        f'\'1\', \'{datetime.now()}\')'
                cur.execute(query)
                conn.commit()


def set_lesson_status_sent(subscription_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'INSERT INTO got_lesson_today ' \
                    f'VALUES (\'{subscription_id}\')'
            cur.execute(query)
            conn.commit()


def reset_lesson_number(subscription_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'UPDATE last_listened SET lesson_number = \'0\' ' \
                    f'WHERE subscription_id = \'{subscription_id}\''
            cur.execute(query)


def if_first_lesson(subscription_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT lesson_number FROM last_listened WHERE ' \
                    f'subscription_id = \'{subscription_id}\''
            cur.execute(query)
            day_in_table = cur.fetchone()
            status = False
            if day_in_table:
                day_number = day_in_table[0]
                if day_number == '0':
                    status = True
            else:
                status = True
            return status


def get_lesson_number(subscription_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT lesson_number FROM last_listened WHERE ' \
                    f'last_listened.subscription_id = \'{subscription_id}\''
            cur.execute(query)
            lesson_number = cur.fetchone()
            if lesson_number:
                lesson_number = lesson_number[0]
            else:
                lesson_number = False
            return lesson_number


def get_course_progress(subscription_id):
    lesson_number = get_lesson_number(subscription_id)
    if not lesson_number:
        lesson_number = '0'
    course_id = subscription_id.split('/')[1]
    folder_name = c.get_folder_name_by_course_id(course_id)
    course_lenght = c.get_course_lenght(folder_name)
    course_progress = f'{lesson_number}/{course_lenght}'
    return course_progress


def set_file_id(file_name, file_id):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT file_id from files_id ' \
                    f'WHERE file_name = \'{file_name}\''
            cur.execute(query)
            file_id_in_db = cur.fetchone()
            if file_id_in_db:
                update_file_id_query = f'UPDATE files_id ' \
                                       f'SET file_id = \'{file_id}\' ' \
                                       f'WHERE file_name = \'{file_name}\''
                cur.execute(update_file_id_query)
                conn.commit()
            else:
                add_file_id_query = f'INSERT INTO files_id ' \
                                    f'VALUES (\'{file_name}\', \'{file_id}\')'
                cur.execute(add_file_id_query)
                conn.commit()


def get_file_id(file_name):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = f'SELECT file_id FROM files_id WHERE ' \
                    f'file_name = \'{file_name}\''
            cur.execute(query)
            file_id = cur.fetchone()
            if file_id:
                file_id = file_id[0]
            return file_id


def get_actual_subscriptions():
    with psycopg2.connect(settings.db_parameters_string) as conn:
        with conn.cursor() as cur:
            query = 'SELECT s.subscription_id ' \
                    'FROM got_lesson_today g ' \
                    'RIGHT JOIN subscriptions s ' \
                    'ON g.subscription_id = s.subscription_id ' \
                    'JOIN last_listened l ' \
                    'ON s.subscription_id = l.subscription_id ' \
                    'WHERE g.subscription_id IS NULL ' \
                    'AND (current_time > publication_time)'
            cur.execute(query)
            subscriptions = cur.fetchall()
            subscribtion_list = list()
            if subscriptions:
                for subscriber in subscriptions:
                    subscribtion_list.append(subscriber[0])
            return subscribtion_list
