import requests
import datetime
import re


holidays = [
    ('Tishri', 1),
    ('Tishri', 2),
    ('Tishri', 10),
    ('Tishri', 15),
    ('Tishri', 16),
    ('Tishri', 22),
    ('Tishri', 23),
    ('Nisan', 15),
    ('Nisan', 16),
    ('Tishri', 21),
    ('Tishri', 22),
    ('Iyar', 6),
    ('Iyar', 7),
    ('Av', 9)
]

now = datetime.datetime.now()
params = {'mode': 'day',
          'timezone': 'Europe/Moscow',
          'dateBegin': f'{now.month}/{now.day}/{now.year}',
          'lat': '55.7',
          'lng': '37.5'
          }
url = 'http://db.ou.org/zmanim/getCalendarData.php'


def check_date():
    holiday = False
    day_of_week = datetime.datetime.today().weekday()
    if day_of_week == 5:
        holiday = True
    else:
        zmanim = requests.get(url, params=params)
        zmanim_dict = zmanim.json()
        month = re.search(r'[a-zA-z]+', zmanim_dict['hebDateString']) \
            .group(0)
        day = re.findall(r'\d+', zmanim_dict['hebDateString'])[0]
        today = (month, day)
        if today in holidays:
            holiday = True
    return holiday

