import psycopg2

import settings


with psycopg2.connect(settings.db_parameters_string) as conn:
    cur = conn.cursor()
    query = 'DELETE FROM got_lesson_today'
    cur.execute(query)
    conn.commit()
