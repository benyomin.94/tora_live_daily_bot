import lessons
import keyboard
import time_state
import db_operations
import csv_operations as c


def subscribe(bot, user, call):
    folder_name = call.data.split('-')[1]
    course_name = c.get_course_name_by_folder_name(folder_name)
    course_id = c.get_course_id_by_folder_name(folder_name)
    subscription_id = f'{user}/{course_id}'
    status = db_operations.subscribe(user, folder_name)
    if status == 'started':
        response = f'Вы подписались на цикл "{course_name}"!\n' \
                   f'Чтобы изменить время публикации уроков, ' \
                   f'зайдите в "Управление подписками".'
        bot.answer_callback_query(call.id, response, show_alert=True)
        bot.send_message(user,
                         'Выберите:',
                         reply_markup=keyboard.get_main_menu())
        lessons.send_audio_lesson(subscription_id)
    elif status == 'continued':
        progress = db_operations.get_course_progress(subscription_id)
        publication_time = db_operations.get_publication_time(subscription_id)
        alert_text = f'Вы возобновили подписку на курс \"{course_name}\"'
        bot.answer_callback_query(call.id, alert_text, show_alert=True)
        edited_response = f'*Название цикла:* {course_name}\n' \
                          f'*Прослушано уроков:* {progress}\n' \
                          f'*Статус подписки:* Активна\n' \
                          f'*Время публикации уроков:* {publication_time}'
        bot.edit_message_text(chat_id=call.message.chat.id,
                              message_id=call.message.message_id,
                              text=edited_response,
                              reply_markup=keyboard.get_status_keyboard('on', folder_name),
                              parse_mode='Markdown'
                              )
    else:
        response = f'Вы уже подписаны на этот курс!'
        bot.answer_callback_query(call.id, response, show_alert=True)


def stop_subscription(bot, user, call):
    folder_name = call.data.split('-')[1]
    course_name = c.get_course_name_by_folder_name(folder_name)
    subscription_id = f'{user}/{c.get_course_id_by_folder_name(folder_name)}'
    progress = db_operations.get_course_progress(subscription_id)
    publication_time = db_operations.get_publication_time(subscription_id)
    db_operations.stop_subscription(user, folder_name)
    alert_text = f'Вы приостановили подписку на курс \"{course_name}\"'
    bot.answer_callback_query(call.id, alert_text, show_alert=True)
    edited_response = f'*Название цикла:* {course_name}\n' \
                      f'*Прослушано уроков:* {progress}\n' \
                      f'*Статус подписки:* Остановлена\n' \
                      f'*Время публикации уроков:* {publication_time}'
    reply_keyboard = keyboard.get_status_keyboard('off', folder_name)
    bot.edit_message_text(chat_id=call.message.chat.id,
                          message_id=call.message.message_id,
                          text=edited_response,
                          reply_markup=reply_keyboard,
                          parse_mode='Markdown'
                          )


def delete_subscription(bot, user, call):
    folder_name = call.data.split('-')[1]
    course_name = c.get_course_name_by_folder_name(folder_name)
    db_operations.delete_subscription(user, folder_name)
    alert_text = f'Вы отписались от цикла "{course_name}"'
    bot.answer_callback_query(call.id, alert_text, show_alert=True)
    bot.delete_message(call.message.chat.id, call.message.message_id)
    subscription_list = db_operations.get_subscription_list(user)
    if not subscription_list:
        bot.send_message(user, 'У вас нет активных подписок!')


def edit_time(bot, user, call):
    folder_name = call.data.split('-')[1]
    response = 'Для того, чтобы получать уроки в удобное для вас время, ' \
               'пожалуйста, введите время в формате ЧЧ:ММ ПО МОСКОВСКОМУ ' \
               'ВРЕМЕНИ.'
    keyboard_markup = keyboard.get_cancel_keyboard()
    time_state.set_time_state(user, folder_name)
    bot.answer_callback_query(call.id)
    bot.send_message(user, response, reply_markup=keyboard_markup)


def reset_day(bot, user, call):
    folder_name = call.data.split('-')[1]
    course_id = c.get_course_id_by_folder_name(folder_name)
    course_name = c.get_course_name_by_id(course_id)
    subscription_id = f'{user}/{course_id}'
    db_operations.reset_lesson_number(subscription_id)
    allert_text = 'Подписка сброшена на первый день!'
    bot.answer_callback_query(call.id, allert_text, show_alert=True)
    response = f'Успех! Теперь вы начнете получать курс "{course_name} "' \
               'с первого урока!'
    progress = db_operations.get_course_progress(subscription_id)
    publication_time = db_operations.get_publication_time(subscription_id)
    edited_message = f'*Название цикла:* {course_name}\n' \
                     f'*Прослушано уроков:* {progress}\n' \
                     f'*Статус подписки:* Активна\n' \
                     f'*Время публикации уроков:* {publication_time}'
    keyboard_markup = keyboard.get_status_keyboard('on',
                                                   folder_name,
                                                   True
                                                   )
    bot.edit_message_text(chat_id=call.message.chat.id,
                          message_id=call.message.message_id,
                          text=edited_message,
                          parse_mode='Markdown',
                          reply_markup=keyboard_markup)
    bot.send_message(user, response)


def handle_callback(bot, user, call):
    payload_prefix = call.data.split('-')[0]
    prefixes = {
        'sub': subscribe,
        'continue': subscribe,
        'stop': stop_subscription,
        'delete': delete_subscription,
        'set_time': edit_time,
        'reset': reset_day
    }
    function_to_do = prefixes[payload_prefix]
    function_to_do(bot, user, call)
