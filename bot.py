import telebot

from tornado import httpserver
from tornado.web import HTTPError
from tornado.escape import json_decode
from tornado.ioloop import IOLoop

import tornado.web

import settings
import keyboard
import text_handler
import db_operations
import callback_handler


WEBHOOK_HOST = '188.42.54.82'
WEBHOOK_PORT = 443
WEBHOOK_URL_BASE = f'{WEBHOOK_HOST}:{WEBHOOK_PORT}'
WEBHOOK_URL_PATH = f'/{settings.TOKEN}/'
WEBHOOK_SSL_CERT = './webhook_cert.pem'
WEBHOOK_SSL_PRIV = './webhook_pkey.pem'


bot = telebot.TeleBot(settings.TOKEN)


class WebhookServer(tornado.web.RequestHandler):
    def post(self):
        headers = self.request.headers
        if 'content-length' in headers and \
            'content-type' in headers and \
            headers['content-type'] == 'application/json':

            json_string = json_decode(self.request.body)
            update = telebot.types.Update.de_json(json_string)
            bot.process_new_updates([update])
        else:
            raise HTTPError(403)


application = tornado.web.Application([
    (WEBHOOK_URL_PATH, WebhookServer),
])


@bot.message_handler(commands=['start'])
def handle_start_command(message):
    db_operations.check_user(message.from_user)
    response = 'Добро пожаловать в ToraLiveDailyBot! \n' \
               'Здесь вы можете подписаться на ежедневные рассылки ' \
               'аудиоуроков на разные темы.'
    bot.send_message(message.from_user.id,
                     response,
                     reply_markup=keyboard.get_main_menu()
                     )


@bot.message_handler(func=lambda message: True, content_types=['text'])
def handle_text(message):
    text_handler.handle_text(bot, message.from_user.id, message.text)


@bot.callback_query_handler(func=lambda call: True)
def handle_callback(call):
    callback_handler.handle_callback(bot, call.from_user.id, call)


if __name__ == '__main__':
    server = httpserver.HTTPServer(
        application,
        ssl_options={
            "certfile": WEBHOOK_SSL_CERT,
            "keyfile": WEBHOOK_SSL_PRIV,
        }
    )

    bot.remove_webhook()
    bot.set_webhook(url=f'{WEBHOOK_URL_BASE}{WEBHOOK_URL_PATH}',
                    certificate=open(WEBHOOK_SSL_CERT, 'r'))
    server.listen(WEBHOOK_PORT)
    IOLoop.instance().start()


