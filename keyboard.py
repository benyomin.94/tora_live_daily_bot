from telebot.types import InlineKeyboardButton
from telebot.types import InlineKeyboardMarkup
from telebot.types import ReplyKeyboardMarkup

def get_main_menu():
    menu = ReplyKeyboardMarkup(True, False)
    menu.row('Выбрать курсы уроков')
    menu.row('Управление подписками')
    return menu


def get_status_keyboard(status, folder_name, first_lesson=False):
    keyboard = InlineKeyboardMarkup()
    stop_button = InlineKeyboardButton(
        text='Приостановить подписку',
        callback_data=f'stop-{folder_name}'
    )
    continue_button = InlineKeyboardButton(
        text='Возобновить подписку',
        callback_data=f'continue-{folder_name}'
    )
    delete_button = InlineKeyboardButton(
        text='Отписаться',
        callback_data=f'delete-{folder_name}'
    )
    reset_button = InlineKeyboardButton(
        text='Начать курс заново',
        callback_data=f'reset-{folder_name}'
    )
    time_button = InlineKeyboardButton(
        text='Изменить время отправки',
        callback_data=f'set_time-{folder_name}'
    )

    if status == 'on':
        keyboard.add(stop_button)
        keyboard.add(delete_button)
        keyboard.add(time_button)
        if not first_lesson:
            keyboard.add(reset_button)
    elif status == 'off':
        keyboard.add(continue_button)
        keyboard.add(delete_button)
        keyboard.add(reset_button)

    return keyboard


def get_time_keyboard(folder_name):
    keyboard = InlineKeyboardMarkup()
    time_button = InlineKeyboardButton(
        text='Ввести время',
        callback_data=f'input_time-{folder_name}'
    )
    keyboard.add(time_button)
    return keyboard


def get_cancel_keyboard():
    keyboard_markup = ReplyKeyboardMarkup(True, False)
    keyboard_markup.row('Отмена')
    return keyboard_markup
