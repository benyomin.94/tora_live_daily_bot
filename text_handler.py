import keyboard
import time_state
import db_operations
import csv_operations as c

from telebot.types import ReplyKeyboardMarkup
from telebot.types import InlineKeyboardButton
from telebot.types import InlineKeyboardMarkup


def choose_courses(bot, user, message):
    courses_list = c.get_courses_list()
    keyboard = ReplyKeyboardMarkup(True, False)
    keyboard.row('Назад')
    response = 'Выберите интересующий вас курс:'
    bot.send_message(user, response, reply_markup=keyboard)
    for course in courses_list:
        course_name = course['course_name']
        folder_name = c.get_folder_name_by_course_name(course_name)
        course_author = course['author']
        number_of_lessons = c.get_course_lenght(folder_name)
        course_description = course['description']
        course_length = course['length']
        course_info_message = f'Название: *{course_name}*\n' \
                              f'Автор: *{course_author}*\n' \
                              f'Количество уроков: *{number_of_lessons}*\n' \
                              f'Средняя продолжительность ' \
                              f'урока: *{course_length}*\n' \
                              f'Описание: \n{course_description}'
        folder_name = c.get_folder_name_by_course_name(course_name)
        course_keyboard = InlineKeyboardMarkup()
        subscribe_button = InlineKeyboardButton(
            text='Подписаться',
            callback_data=f'sub-{folder_name}'
        )
        course_keyboard.row(subscribe_button)
        bot.send_message(
            user,
            course_info_message,
            reply_markup=course_keyboard,
            parse_mode='Markdown'
        )


def send_course_info(bot, user, course_name):
    response = c.get_course_description_by_course_name(course_name)
    keyboard = InlineKeyboardMarkup()
    folder_name = c.get_folder_name_by_course_name(course_name)
    subscribe_button = InlineKeyboardButton(text='Подписаться',
                                            callback_data=f'sub-{folder_name}')
    keyboard.add(subscribe_button)
    bot.send_message(user, response, reply_markup=keyboard)


def send_main_menu(bot, user, message):
    response = 'Выберите:'
    bot.send_message(user,
                     response,
                     reply_markup=keyboard.get_main_menu()
                     )


def send_course_settings(bot, user, message):
    subs_list = db_operations.get_subscription_list(user)
    if subs_list:
        for sub in subs_list:
            course_name = sub['course_name']
            folder_name = c.get_folder_name_by_course_name(course_name)
            course_id = c.get_course_id_by_folder_name(folder_name)
            subscription_id = f'{user}/{course_id}'
            first_lesson = db_operations.if_first_lesson(subscription_id)
            progress = db_operations.get_course_progress(subscription_id)
            status_dict = {
                'on': 'Активна',
                'off': 'Остановлена'
            }
            status = status_dict[sub['status']]
            time = sub['time']
            reply_keyboard = keyboard.get_status_keyboard(sub['status'],
                                                          folder_name,
                                                          first_lesson
                                                          )
            response = f'*Название цикла:* {course_name}\n' \
                       f'*Прослушано уроков:* {progress}\n'\
                       f'*Статус подписки:* {status}\n' \
                       f'*Время публикации уроков:* {time}'
            bot.send_message(user,
                             response,
                             reply_markup=reply_keyboard,
                             parse_mode='Markdown')
    else:
        bot.send_message(user, 'У вас нет активных подписок!')


def parse_time(time):
    response = False
    try:
        time = time.split(':')
        if len(time) == 2:
            if len(time[0]) <= 2 and len(time[1]) <= 2 and \
                    time[0].isdigit() and time[1].isdigit() and\
                    int(time[0]) < 24 and int(time[1]) < 60:
                response = f'{time[0]}:{time[1]}'
    except:
        pass
    return response


def incorrect_text(bot, user, message):
    response = 'Некорректная команда. Пожалуйста, выберите один из ' \
               'вариантов на кнопках.'
    bot.send_message(user, response)


def cancel(bot, user):
    time_state.delete_time_state(user)
    reply_keyboard = keyboard.get_main_menu()
    response = 'Выберите:'
    bot.send_message(user, response, reply_markup=reply_keyboard)


def handle_text(bot, user, message):
    waiting_for_time = time_state.check_time_state(user)
    if waiting_for_time:
        if message == 'Отмена':
            cancel(bot, user)
        else:
            time = parse_time(message)
            if time:
                subscription_id = waiting_for_time[1]
                course_id = subscription_id.split('/')[1]
                course_name = c.get_course_name_by_id(course_id)
                db_operations.edit_time(subscription_id, time)
                time_state.delete_time_state(user)
                response = f'Готово! Теперь уроки из цикла {course_name} вы ' \
                           f'будете получать в {time} по московскому времени.'
                reply_keyboard = keyboard.get_main_menu()
                bot.send_message(user, response, reply_markup=reply_keyboard)
            else:
                response = 'Пожалуйста, введите время в корректном формате!'
                keyboard_markup = keyboard.get_cancel_keyboard()
                bot.send_message(user, response, reply_markup=keyboard_markup)
    else:
        messages = {
            'Выбрать курсы уроков': choose_courses,
            'Законы достойной речи': send_course_info,
            'Управление подписками': send_course_settings,
            'Назад': send_main_menu

        }
        function = messages.get(message, incorrect_text)
        function(bot, user, message)
