import settings
import db_operations
import csv_operations as c

from telebot import TeleBot


def send_audio_lesson(subscription_id):
    bot = TeleBot(settings.TOKEN)
    user = subscription_id.split('/')[0]
    course_id = subscription_id.split('/')[1]
    folder_name = c.get_folder_name_by_course_id(course_id)
    course_name = c.get_course_name_by_id(course_id)
    lesson_dict = db_operations.get_lesson_dict(subscription_id)
    file_name = lesson_dict['filename']
    day = file_name.split('_')[1]
    performer = c.get_course_author_by_id(course_id)
    file_id = db_operations.get_file_id(file_name)
    annotation = lesson_dict['annotation']
    annotation_str = f'*{course_name}, день {day}*\n\n' \
                     f'{annotation}'
    bot.send_message(user, annotation_str, parse_mode='Markdown')
    if file_id:
        bot.send_audio(user, file_id, title=lesson_dict['annotation'])
        
    else:
        with open(f'courses/{folder_name}/{file_name}.mp3', 'rb') as lesson:
            msg = bot.send_audio(
                user,
                lesson,
                title=lesson_dict['lesson_name'],
                performer=performer
            )
            new_file_id = msg.audio.file_id
            db_operations.set_file_id(file_name, new_file_id)
    db_operations.increment_lesson_number(subscription_id)
    db_operations.set_lesson_status_sent(subscription_id)
