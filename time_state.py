import psycopg2

import settings
import csv_operations


def check_time_state(user):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        cur = conn.cursor()
        check_query = f'SELECT * FROM time_state ' \
                      f'WHERE time_state.user = \'{user}\''
        cur.execute(check_query)
        waiting_for_time = cur.fetchone()
        status = None
        if waiting_for_time:
            status = waiting_for_time
        return status


def set_time_state(user, folder_name):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        cur = conn.cursor()
        course_id = csv_operations.get_course_id_by_folder_name(folder_name)
        subscription_id = f'{user}/{course_id}'
        check_query = f'SELECT * FROM time_state WHERE user = \'{user}\''
        cur.execute(check_query)
        user_in_db = cur.fetchone()
        if not user_in_db:
            set_time_query = f'INSERT INTO time_state ' \
                             f'VALUES (\'{user}\', \'{subscription_id}\')'
            cur.execute(set_time_query)
            conn.commit()
        else:
            pass


def delete_time_state(user):
    with psycopg2.connect(settings.db_parameters_string) as conn:
        cur = conn.cursor()
        delete_time_query = f'DELETE FROM time_state ' \
                            f'WHERE time_state.user = \'{user}\''
        cur.execute(delete_time_query)
        conn.commit()
